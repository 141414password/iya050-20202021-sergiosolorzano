import React from "react"
import SignIn from "./components/sign-in.jsx"

const App = () => (
  //--> elemento padre que agupoa otros elementos dentro en uno, se llama un fragmento.
  //voy a necesitar ReactRouter aquí
  <> 
    <h1> hello </h1>
    <p> This is a demo </p>
    <SignIn/>
  </>
);

export default App