const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    index: './src/index.js',
  },
 devtool: 'inline-source-map',
 devServer: {
  contentBase: './dist',
},
plugins: [
  new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),
  new HtmlWebpackPlugin({
    template: "./src/index.html",
  }),
],
output: {
  filename: 'bundle.js',
  path: path.resolve(__dirname, 'dist'),
},
    module: {
      rules: [
        {
          test: /.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env','@babel/preset-react']
            }
          }
        }
      ]
    }
  
};